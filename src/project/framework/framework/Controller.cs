﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace framework
{
    class Controller
    {
        public static int UpperLimit = -1;
        public static int LowerLimit = -1;
        public static string DataDirectory;
        public static string OutputDirectory;
		public static string Separator = ""+System.IO.Path.DirectorySeparatorChar;

        public delegate List<OneImage> Algorithm(OneLocation keyword);

        static public Algorithm CurrentAlgorithm;

        static public List<OneImage> RunAlgorithm(OneLocation keyword)
        {
            return CurrentAlgorithm(keyword);
        }

        static public List<OneImage> DummyAlgorithm(OneLocation keyword)
        {
            return keyword.Images;
        }

		static public void WriteRunningResultToFile(List<OneImage> resultList,string locationName,int locationNum)
        {
			using (StreamWriter writer = new StreamWriter(OutputDirectory + Separator + locationName + ".out", false))
            {
				//export only 50 lines
				int maxLines = 50;
				int currLines = 0;

				foreach (var result in resultList)
                {
					writer.WriteLine(locationNum + "\tQ0\t" + result.ID + "\t" + currLines + "\t" + (maxLines-currLines) + "\tignoreit");
					currLines++;
					if (currLines==maxLines)
						break;
                }
            }
        }
		/// <summary>
		/// Reads the location list.
		/// </summary>
		/// <returns>The location list.</returns>
		/// <param name="LocationListName">Location list name.</param>
		/// <param name="LowerNumber">Lower number.</param>
		/// <param name="UpperNumber">Upper number.</param>
        static public List<OneLocation> ReadLocationList(string LocationListName, int LowerNumber = 0, int UpperNumber = 0) 
        {
            List < OneLocation > keywords = new List<OneLocation>();
            var keywordXML = XDocument.Load(LocationListName);

            foreach (var topic in keywordXML.Root.Descendants("topic"))
            {
                double d = -1;
                bool b = false;
				
				if (UpperNumber-- == 0) 
                    return keywords;

                if (!(LowerNumber-- > 0))
                {
                    OneLocation tmp = new OneLocation();
                    foreach (var t in topic.Descendants())
                    {
                        switch (t.Name.LocalName)
                        {
                            case "number":
                                tmp.Num = int.Parse(t.Value);
                                break;
                            case "title":
                                tmp.Title = t.Value;
                                break;
                            case "latitude":
                                 d = -1;
                                 b = Double.TryParse(t.Value, out d);
                                 if (!b)
                                 {
                                     b = Double.TryParse(t.Value.Replace(".", ","), out d);
                                 }
                                 tmp.Latitude = d;
                                break;
                              // tmp.Latitude = double.Parse(t.Value);
                            case "longitude":
                                 d = -1;
                                 b = Double.TryParse(t.Value, out d);
                                 if (!b)
                                 {
                                     b = Double.TryParse(t.Value.Replace(".", ","), out d);
                                 }
                                 tmp.Longitude = d;
                               // tmp.Longitude = double.Parse(t.Value);
                                break;
                            case "wiki":
                                tmp.Wiki = t.Value;
                                break;
                            default:
                                break;
                        }
                    }
                    keywords.Add(tmp);
                }
            }

            return keywords;
        }

		/// <summary>
		/// Gets a specific line from the csv file, which has the imageid as first item
		/// </summary>
		/// <returns>The line for a specific image in the csv file, if found/returns>
		/// <param name="csvdata">csv file data</param>
		/// <param name="imageid">the image id we are searching for</param>
		static private List<string> getLineForImageFromCsv(List<string[]> csvdata, long imageid)
		{
			foreach (var line in csvdata) {
				if (long.Parse (line [0]) == imageid)
					return new List<string>(line);
			}

			return new List<string> ();
		}

        /// <summary>
        /// Reads the locations into OneLocation list
        /// </summary>
        /// <returns>The locations.</returns>
        /// <param name="DirectoryPath">Directory path.</param>
        /// <param name="Location">Location.</param>
        static public List<OneLocation> ReadLocations(string DirectoryPath, List<OneLocation> Location)
        {
            double d = -1;
            bool b = false;


			// each LOCATION
			foreach (var location in Location)
            {
				//image list
                List<OneImage> results = new List<OneImage>();

				//load 
				var faceCSV=ReadCSVFileIntoArray (DirectoryPath + Controller.Separator + "descvis" + Controller.Separator + "img" + Controller.Separator + location.Title + " FACE.csv");
				var cnCSV=ReadCSVFileIntoArray (DirectoryPath + Controller.Separator + "descvis" + Controller.Separator + "img" + Controller.Separator + location.Title + " CN.csv");
				var cmCSV=ReadCSVFileIntoArray (DirectoryPath + Controller.Separator + "descvis" + Controller.Separator + "img" + Controller.Separator + location.Title + " CM.csv");

				//parse the location xml
				var keywordXML = XDocument.Load(DirectoryPath + Controller.Separator + "xml"+ Controller.Separator + location.Title + ".xml" );

                foreach (var photo in keywordXML.Root.Descendants())
                {
                    OneImage currImg = new OneImage();

					//some retarded xml parser
                    foreach (var t in photo.Attributes())
                    {
                        switch (t.Name.LocalName)
                        {
                            case "views":
                                currImg.Views = int.Parse(t.Value);
                                break;
                            case "title":
                                currImg.Title = t.Value;
                                break;
                            case "username":
                                currImg.Username = t.Value;
                                break;
                            case "url_b":
                                currImg.Url_b = t.Value;
                                break;
                            case "tags":
                                currImg.Tags = t.Value;
                                break;
                            case "rank":
                                currImg.Rank = int.Parse(t.Value);
                                break;
                            case "nbComments":
                                currImg.NbOfComments = int.Parse(t.Value);
                                break;
                            case "longitude":
                                d = -1;
                                b = Double.TryParse(t.Value, out d);
                                if (!b)
                                {
                                    b = Double.TryParse(t.Value.Replace(".", ","), out d);
                                }
                                currImg.Longitude = d;
                                break;
                            case "license":
                                currImg.License = int.Parse(t.Value);
                                break;
                            case "latitude":
                                d = -1;
                                b = Double.TryParse(t.Value, out d);
                                if (!b)
                                {
                                    b = Double.TryParse(t.Value.Replace(".", ","), out d);
                                }
                                currImg.Latitude = d;
                                break;
                            case "id":
                                currImg.ID = long.Parse(t.Value);
                                break;
                            case "description":
                                currImg.Description = t.Value;
                                break;
                            case "date_taken":
                                currImg.SetTimeFromString(t.Value);
                                break;
                            default:
                                break;
                        }
                    }

					//parse face
					{
						var faceLine = getLineForImageFromCsv (faceCSV, currImg.ID);
						currImg.ImgFace = double.Parse (faceLine [1]);
					}
					//parse CN
					{
						var cnLine = getLineForImageFromCsv (cnCSV, currImg.ID);
						double[] dCn = new double[11];
						for (int i=0; i<11; i++) {
							dCn [i] = double.Parse (cnLine [1 + i]);
						}
						currImg.ImgCN = dCn;
					}
					//parse CM
					{
						var cmLine = getLineForImageFromCsv (cmCSV, currImg.ID);
						double[] dCm = new double[9];
						for (int i=0; i<9; i++) {
							dCm [i] = double.Parse (cmLine [1 + i]);
						}
						currImg.ImgCM = dCm;
					}

                    results.Add(currImg);
                }
                location.Images = results;
            }

            return Location;
        }

        static public void ProcessLocations(List<OneLocation> locations)
        {
			// Process the locations one by one
            foreach (var loc in locations)
            {
				var res=RunAlgorithm (loc);
				if (res.Count > 0) {
					WriteRunningResultToFile (res, loc.Title, loc.Num);
				}
            }
        }

        //Keep in mind that values are accessed by the column number, and then the row number like in a regular matrix.
		// Baszod. Ideszarsz egy google találatot arról, hogy kell csv-t betölteni, ez gecire nem az, 
		// mint megírni az adatok betöltését.... Faszom.... (by Zsombor)
        static public List<string[]> ReadCSVFileIntoArray(string filePath,string splitcharacter=",")
        {
            string CSVFileContents = "";

            using (StreamReader oStreamReader = new StreamReader(File.OpenRead(filePath)))
            {
                CSVFileContents = oStreamReader.ReadToEnd();
            }

            List<string[]> CSVList = new List<string[]>();

            string[] sFileLines = CSVFileContents.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string sFileLine in sFileLines)
            {
				CSVList.Add(sFileLine.Split(splitcharacter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
            }

            return CSVList;
        }
    }
}
