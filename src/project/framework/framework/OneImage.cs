using System;

namespace framework
{
	public class OneImage
	{
		public string Title { get; set; }
		public int Views { get; set; }
		public string Username { get; set; }
		public string Url_b { get; set; }
		public string Tags { get; set; }
		public int Rank { get; set; }
		public int NbOfComments { get; set; }
		public double Latitude { get; set; }
		public int License { get; set; }
		public double Longitude { get; set; }
		public long ID { get; set; }
		public string Description { get; set; }
		public DateTime DateTaken { get; set; }

		// image descriptors start
		public double ImgFace { get; set; }
		public double[] ImgCN { get; set; }
		public double[] ImgCM { get; set; }
		// image descriptors end


		public OneImage(){}

		public bool SetTimeFromString(string Date)
		{
			if (Date.Equals(""))
			{
				DateTaken = DateTime.Now;
			}
			else
			{
				DateTime tmp;
				if (DateTime.TryParse(Date, out tmp))
				{
					this.DateTaken = tmp;
				}
				else
				{
					this.DateTaken = DateTime.Now;
				}
			}
			return true;
		}

	}
}

