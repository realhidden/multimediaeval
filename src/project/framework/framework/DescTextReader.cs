﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using framework;

namespace desctext_reader
{

    // This method is responsible to read the tfidf, socialtfidf, probabilistic files
    // and to create  a list, which contains the location and a list of the tags related to the location with the value belonging to the tag

    class DescTextLocTagValueBuilder
    {
        public static string Separator = "" + System.IO.Path.DirectorySeparatorChar;
        public static string foldername = "desctxt";

        static public List<List<descTags>> descTagsReader()
        {

            string[] tagSplitted;
            List<List<descTags>> descTagsListList = new List<List<descTags>>();

            try
            {
                string directorypath = Controller.DataDirectory + Separator + foldername;
                string[] sa = Directory.GetFiles(directorypath);

                foreach (string filepath in sa)
                {
                    List<descTags> descTagsList = new List<descTags>();
                    string[] filecontent = File.ReadAllLines(filepath);
                    foreach (string line in filecontent)
                    {
                        string[] lineSplitted = line.Split('\t');
                        descTags mc = new descTags() { Name = lineSplitted[0] };
                        for (int i = 1; i < lineSplitted.Length; i++)
                        {
                            tagSplitted = lineSplitted[i].Split(' ');
                            string tag = tagSplitted[0];
                            double d = -1;
                            bool b = Double.TryParse(tagSplitted[1], out d);
                            if (!b)
                            {
                                b = Double.TryParse(tagSplitted[1].Replace(".", ","), out d);
                            }
                            if (!b)
                            {
                                ////
                            }
                            mc.TagDictionary.Add(tag, d);
                        }
                        descTagsList.Add(mc);
                    }
                    descTagsListList.Add(descTagsList);
                }

            }
            catch (IOException ex)
            {
            }
            catch (Exception ex)
            {
            }



            return descTagsListList;
        }
    }

    class descTags
    {
        private string _name;
        public string Name { get { return _name; } set { _name = value; } }
        public Dictionary<string, double> TagDictionary { get; set; }

        public descTags()
        {
            TagDictionary = new Dictionary<string, double>();
        }
    }
}