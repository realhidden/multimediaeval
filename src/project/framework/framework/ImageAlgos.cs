using System;
using System.Collections.Generic;
using System.Linq;

namespace framework
{
	public delegate double DistanceDelegate(Cluster a,Cluster b);

	public class Cluster{
		public List<OneImage> items=new List<OneImage>();

		public void mergeCluster(Cluster b)
		{
			items.InsertRange (0, b.items);
		}

		public static List<Cluster> doClustering(DistanceDelegate distance, List<OneImage> images, int lowerlimit)
		{
			//fill the initial cluster
			List<Cluster> allc=new List<Cluster>();
			foreach(var i in images)
			{
				Cluster c=new Cluster();
				c.items.Add(i);
				allc.Add(c);
			}

			//woo
			while (allc.Count>lowerlimit)
			{
				double minDist=distance(allc[0],allc[1]);
				Cluster pairA=allc[0];
				Cluster pairB=allc[1];

				//iterate me plz
				for (int i=0; i<allc.Count; i++) {
					for (int j=i+1; j<allc.Count; j++) {
						var currdist=distance(allc [i],allc [j]);
						if (currdist < minDist) {
							minDist=currdist;
							pairA = allc [i];
							pairB = allc [j];
						}
					}
				}

				//merge
				pairA.mergeCluster(pairB);
				allc.Remove(pairB);
			}

			return allc;
		}

		public static int getImageClusterId(List<Cluster> clusters,OneImage img)
		{
			for (int i=0; i<clusters.Count; i++) {
				if (clusters [i].items.IndexOf (img) != -1)
					return i;
			}
			return -1;
		}
	}

	static public class ImageAlgos
	{
		/// <summary>
		/// Sorts the images by rank.
		/// </summary>
		/// <returns>sorter int</returns>
		/// <param name="a">an image to compare</param>
		/// <param name="b">an image to compare</param>
		static public int SortImagesByRank(OneImage a, OneImage b)
		{
			if (a.Rank == b.Rank)
				return 0;

			return a.Rank > b.Rank ? 1 : -1;
		}

		static public void normalizeRanks(List<OneImage> list)
		{
			int id = 0;
			foreach(var i in list)
			{
				i.Rank=id;
				id++;
			}
		}

		static public List<OneImage> TextVis(OneLocation loc)
		{
			var result=desctext_reader.DescTextAlgorithm.ImageRankByDescText(loc);
			loc.Images = result;
			return HierCnPlusFaceAlgorithm (loc);
		}

		static public List<OneImage> NoFaceAlgorithm(OneLocation location)
		{
			List<OneImage> result = location.Images;

			//if face > 0, give the image rank a +1000 boost
			foreach (var oneImg in result) {
				if (oneImg.ImgFace > 0.0f) {
					oneImg.Rank += 1000;
				}
			}

			// sort the result
			result.Sort (SortImagesByRank);
			return result;
		}

		static public List<OneImage> HierCnAlgorithm(OneLocation location)
		{
			return HierCnAlgorithmImages(location.Images);
		}

		static public List<OneImage> HierCnAlgorithmImages(List<OneImage> locimages)
		{
			// hier10 - 10
			// hier20 - 20
			const int clusterCount=20;			

			List<OneImage> result = locimages;

			//create 10 clusters
			var clusters=Cluster.doClustering((Cluster a,Cluster b) => {

				//calc averages
				List<double>[] cnA=new List<double>[11];
				List<double>[] cnB=new List<double>[11];
				for (int i=0;i<11;i++)
				{
					cnA[i]=new List<double>();
					cnB[i]=new List<double>();
				}

				foreach(var oneA in a.items){
					for (int i=0;i<11;i++)
					{
						cnA[i].Add(oneA.ImgCN[i]);
					}
				}

				foreach(var oneB in b.items){
					for (int i=0;i<11;i++)
					{
						cnB[i].Add(oneB.ImgCN[i]);
					}
				}

				//sum average distances

				double sumDistDelta=0;
				for (int i=0;i<11;i++)
				{

					sumDistDelta+=Math.Abs(cnA[i].Average()-cnB[i].Average());
				}

				return sumDistDelta;
			},result,clusterCount);



			//filter out the "first 10" items of clusters
			for (int i=0; i<clusterCount; i++) {
				foreach (var r in result) {
					if (Cluster.getImageClusterId (clusters, r) == i) {
						//got first one, modify Rank
						r.Rank = -500 + r.Rank;
						break;
					}
				}
			}

			// sort the result
			result.Sort (SortImagesByRank);
			return result;
		}

		static public List<OneImage> HierCnPlusFaceAlgorithm(OneLocation location)
		{
			var ret=NoFaceAlgorithm(location);
			var ret2=HierCnAlgorithmImages(ret);
			return ret2;
		}

		static public List<OneImage> ClustCm(OneLocation location)
		{
			const int clusterCount=20;			

			List<OneImage> result = location.Images;

			//create 10 clusters
			var clusters=Cluster.doClustering((Cluster a,Cluster b) => {

				//calc averages
				List<double>[] cmA=new List<double>[9];
				List<double>[] cmB=new List<double>[9];
				for (int i=0;i<9;i++)
				{
					cmA[i]=new List<double>();
					cmB[i]=new List<double>();
				}

				foreach(var oneA in a.items){
					for (int i=0;i<9;i++)
					{
						cmA[i].Add(oneA.ImgCM[i]);
					}
				}

				foreach(var oneB in b.items){
					for (int i=0;i<9;i++)
					{
						cmB[i].Add(oneB.ImgCM[i]);
					}
				}

				//sum average distances

				double sumDistDelta=0;
				for (int i=0;i<9;i++)
				{
					double delta=Math.Abs(cmA[i].Average()-cmB[i].Average());

					if (i<3){
						sumDistDelta+=delta;
					}else if(i<6){
						sumDistDelta+=delta/30;
					}else{
						sumDistDelta+=delta/900;
					}
				}

				return sumDistDelta;
			},result,clusterCount);



			//filter out the "first 10" items of clusters
			for (int i=0; i<clusterCount; i++) {
				foreach (var r in result) {
					if (Cluster.getImageClusterId (clusters, r) == i) {
						//got first one, modify Rank
						r.Rank = -500 + r.Rank;
						break;
					}
				}
			}

			// sort the result
			result.Sort (SortImagesByRank);
			return result;
		}
	

	static public List<OneImage> ClustModCm(OneLocation location)
	{
		const int clusterCount=10;			

		List<OneImage> result = location.Images;

		//create 10 clusters
		var clusters=Cluster.doClustering((Cluster a,Cluster b) => {

			//calc averages
			List<double>[] cmA=new List<double>[9];
			List<double>[] cmB=new List<double>[9];
			for (int i=0;i<9;i++)
			{
				cmA[i]=new List<double>();
				cmB[i]=new List<double>();
			}

			foreach(var oneA in a.items){
				for (int i=0;i<9;i++)
				{
					cmA[i].Add(oneA.ImgCM[i]);
				}
			}

			foreach(var oneB in b.items){
				for (int i=0;i<9;i++)
				{
					cmB[i].Add(oneB.ImgCM[i]);
				}
			}

			//sum average distances

			double sumDistDelta=0;
			for (int i=0;i<9;i++)
			{
				double delta=Math.Abs(cmA[i].Average()-cmB[i].Average());

				if (i<3){
					sumDistDelta+=delta;
				}else if(i<6){
					sumDistDelta+=delta/30;
				}else{
					sumDistDelta+=delta/900;
				}
			}

			return sumDistDelta;
		},result,clusterCount);



		//filter out the "first 10" items of clusters
		for (int i=0; i<clusterCount; i++) {
			foreach (var r in result) {
				if (Cluster.getImageClusterId (clusters, r) == i) {
					//got first one, modify Rank
					r.Rank = r.Rank - 3;
					break;
				}
			}
		}

		// sort the result
		result.Sort (SortImagesByRank);
		return result;
	}
}
}

