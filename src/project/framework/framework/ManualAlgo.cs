using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace framework
{
	public class ManualCluster{
		public Stack<long> normals{ get; set; }
		public Stack<long> irrelevant{ get; set; }

		public ManualCluster(){
			normals = new Stack<long> ();
			irrelevant = new Stack<long> ();
		}
	}

	public class ManualAlgo
	{
		static string dataDir="";

		static public void SetDataDirectory(string dir)
		{
			dataDir = dir;
		}

		static public OneImage getImageFromLocation(OneLocation loc,long imageid)
		{
			foreach(var i in loc.Images)
			{
				if (i.ID==imageid)
					return i;
			}
			return new OneImage ();
		}

		static public List<OneImage> ManualAlgorithm(OneLocation location)
		{
			//clusters
			List<ManualCluster> clusts = new List<ManualCluster> ();

			//relevant or not
			Hashtable relevants=new Hashtable();

			//loading rGT, dGT
			if (!System.IO.File.Exists (dataDir + Controller.Separator + "gt" + Controller.Separator + "rGT" + Controller.Separator + location.Title + " rGT.csv")) {
				return new List<OneImage> ();
			}
			var rGT = Controller.ReadCSVFileIntoArray (dataDir + Controller.Separator + "gt" + Controller.Separator + "rGT" + Controller.Separator + location.Title + " rGT.csv", " ");
			var dGT = Controller.ReadCSVFileIntoArray (dataDir + Controller.Separator + "gt" + Controller.Separator + "dGT" + Controller.Separator + location.Title + " dGT.csv", " ");

			//reading relevancy
			foreach (var oneD in rGT) {
				int relevant = int.Parse (oneD [1]); //0 - nem relevans
				long imageid = long.Parse (oneD [0]);

				relevants.Add (imageid, relevant==0 ? Boolean.FalseString : Boolean.TrueString);

			}

			//processing images (clustering)
			foreach (var oneD in dGT) {
				int clustId = int.Parse (oneD [1]);
				long imageid = long.Parse (oneD [0]);

				//should be an existing one
				while (clusts.Count<=clustId) {
					clusts.Add (new ManualCluster ());
				}

				if (relevants [imageid] == Boolean.TrueString) {
					clusts [clustId].normals.Push (imageid);
				} else {
					clusts [clustId].irrelevant.Push (imageid);
				}
			}

			//got clusters with normals / irrelevants, now need to reassemble a result line
			int currentCluster = 0;
			List<OneImage> result = new List<OneImage> ();

			//only normals
			while (true) {
				//check if has any normals
				int allNormals = 0;
				foreach (var c in clusts) {
					allNormals += c.normals.Count;
				}
				//if not, break
				if (allNormals == 0)
					break;

				//get the currentCluster and grab item if exists
				if (clusts[currentCluster].normals.Count > 0) {
					//yolo mode
					result.Add(getImageFromLocation(location,clusts[currentCluster].normals.Pop()));
				} else {
					currentCluster ++;
					if (currentCluster > clusts.Count)
						currentCluster = 0;
				}
			}

			//grab all the irrelevants
			foreach (var c in clusts) {
				foreach (var id in c.irrelevant) {
					result.Add (getImageFromLocation (location, id));
				}
			}


			return result;
		}
	}
}

