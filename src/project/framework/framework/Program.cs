﻿using System;
using System.Collections.Generic;
using desctext_reader;

namespace framework
{
    class Program
    {

        static void Help()
        {
			Console.WriteLine ("\nYou can use these commandline parameters:\n\n" +
				"-h\tHelp, shows this message.\n" +
				"-d\tSets the working directory, where the data is.\n" + 
				"-o\tSets the output directory.\n" + 
				"-ll\tLimits the number of the processed keywords. Lower limit.\n" +
				"-lu\tUpper limit\n" +
				"-a\tYou can provide the algorithm to use here.\n");
            Environment.Exit(0);
        }

        static void MenuMaker(string[] args)
        {
            string modifier = "";
            if (args.Length < 1)
            {
                Help();
            }
            foreach (var arg in args)
            {
                switch (arg)
                {
                    case "-h":
                        Help();
                        break;
                    case "-d":
                        modifier = arg;
                        break;
                    case "-o":
                        modifier = arg;
                        break;
                    case "-lu":
                        modifier = arg;
                        break;
                    case "-ll":
                        modifier = arg;
                        break;
                    case "-a":
                        modifier = arg;
                        break;
                    default:
                        switch (modifier)
                        {
                            case "-d":
                                Controller.DataDirectory = arg;
                                break;
                            case "-o":
                                Controller.OutputDirectory = arg;
                                break;
                            case "-lu":
                                if (!int.TryParse(arg, out Controller.UpperLimit))
                                    Help();
                                break;
                            case "-ll":
                                if (!int.TryParse(arg, out Controller.LowerLimit))
                                    Help();
                                break;
                            case "-a":
                                switch (arg)
                                {
                                    case "dummy":
                                        Controller.CurrentAlgorithm = Controller.DummyAlgorithm;
                                        break;
									case "noface":
										Controller.CurrentAlgorithm = ImageAlgos.NoFaceAlgorithm;
										break;
									case "hiercn":
										Controller.CurrentAlgorithm = ImageAlgos.HierCnAlgorithm;
										break;
									case "hiercnface":
										Controller.CurrentAlgorithm = ImageAlgos.HierCnPlusFaceAlgorithm;
										break;
									case "clustcm":
										Controller.CurrentAlgorithm = ImageAlgos.ClustCm;
										break;
									case "clustmodcm":
										Controller.CurrentAlgorithm = ImageAlgos.ClustModCm;
										break;
									case "textdescalgo":
                                        Controller.CurrentAlgorithm = DescTextAlgorithm.ImageRankByDescText;
                                        break;
									case "manual":
										Controller.CurrentAlgorithm = ManualAlgo.ManualAlgorithm;
										break;
									case "textvis":
										Controller.CurrentAlgorithm = ImageAlgos.TextVis;
										break;
                                    default:
                                        Console.WriteLine("\n!! No such algorithm:" + arg + "\tthe dummy algorithm will be used this time.\n");
                                        Controller.CurrentAlgorithm = Controller.DummyAlgorithm;
                                        break;
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                }
            }
        }

        static void Main(string[] args)
        {
            MenuMaker(args);
            List<OneLocation> locations = new List<OneLocation>();

			//find the "topics" xml
			var fileEntries = System.IO.Directory.GetFiles(Controller.DataDirectory);
			string xmlFile = ""; 
			foreach (String entry in fileEntries) {
				if (entry.Contains ("_topics.xml")) {
					xmlFile = entry;
					break;
				}
			}

			//small hack for manual algo
			ManualAlgo.SetDataDirectory(Controller.DataDirectory);

			//read locations
            locations = Controller.ReadLocationList(xmlFile, Controller.LowerLimit, Controller.UpperLimit);
			Controller.ReadLocations(Controller.DataDirectory, locations);
            Controller.ProcessLocations(locations);
        }
    }
}
