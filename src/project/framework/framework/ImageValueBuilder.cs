﻿using System;
using System.IO;
using System.Collections.Generic;
using framework;
using desctext_reader;
using System.Linq;
using System.Text;

namespace desctext_reader
{
    static class Constants
    {
        public const int devsetkeywords_probabilistic = 0;
        public const int devsetkeywords_social_tfidf = 1;
        public const int devsetkeywords_tfidf = 2;
        public const int mod_devsetkeywords_probabilistic = 3;
        public const int mod_devsetkeywords_social_tfidf = 4;
        public const int mod_devsetkeywords_tfidf = 5;

    }

    class ImageValueBuilder
    {
        static public LocationResults BuildImageValues(OneLocation location, List<List<descTags>> descTagsListList)
        {
            List<List<descTags>> mydescTagsListList = descTagsListList;
            OneLocation myLocation = location;
            IndexKeeper ik = new IndexKeeper();
            ik = IndexKeeper.FindIndex(myLocation, mydescTagsListList);

            LocationImageTags simplelocation = new LocationImageTags();
       //     framework.PythonTest.LocImgTagsCreator(myLocation);
            simplelocation = LocImgTagsCreator(myLocation);

            string onetag = "";

            LocationResults locRes = new LocationResults();

            locRes.Title = simplelocation.Title;
            locRes.Num = simplelocation.Num;
            locRes.Latitude = simplelocation.Latitude;
            locRes.Longitude = simplelocation.Longitude;
            locRes.Wiki = simplelocation.Wiki;

            int secIndex = IndexKeeper.getTextDescSecondIndex(ik);
            int loc_Num = simplelocation.Num;
            string loc_Title = simplelocation.Title;
            List<ImageResults> ListImgR = new List<ImageResults>();

            foreach (var simpleimage in simplelocation.imagetagsListList)
            {
                int counter = simpleimage.imgtags.Count;
                ImageResults imgR = new ImageResults();


                double tfidf_value_norm_max = 0.0;
                double tfidf_value_mod_max = 0.0;
                double socialtfidf_value_norm_max = 0.0;
                double socialtfidf_value_mod_max = 0.0;
                double probabilistic_value_norm_max = 0.0;
                double probabilistic_value_mod_max = 0.0;


                // tfidf_summa_norm is calculated by using the original keywords in the original textdesc (add them)
                // tfidf_summa_mod is calculated by using the original keywords in the modified textdesc (add them) 
                double tfidf_summa_norm = 0.0;
                double tfidf_summa_mod = 0.0;
                double socialtfidf_summa_norm = 0.0;
                double socialtfidf_summa_mod = 0.0;
                double probabilistic_summa_norm = 0.0;
                double probabilistic_summa_mod = 0.0;

                // tfidf_mean_norm = tfidf_summa_norm / (number of the tags of the image)
                // tfidf_mean_mod = tfidf_summa_norm / (number of the tags of the image)
                double tfidf_mean_norm = 0.0;
                double tfidf_mean_mod = 0.0;
                double socialtfidf_mean_norm = 0.0;
                double socialtfidf_mean_mod = 0.0;
                double probabilistic_mean_norm = 0.0;
                double probabilistic_mean_mod = 0.0;

                // in case of maxweigh we use also the max tf-idf value of the tags
                // tfidf_mean_norm_maxweight = tfidf_value_norm_max + log(tfidf_mean_norm)
                double tfidf_mean_norm_maxweight = 0.0;
                double tfidf_mean_mod_maxweight = 0.0;
                double socialtfidf_mean_norm_maxweight = 0.0;
                double socialtfidf_mean_mod_maxweight = 0.0;
                double probabilistic_mean_norm_maxweight = 0.0;
                double probabilistic_mean_mod_maxweight = 0.0;

                foreach (var simpletag in simpleimage.imgtags)
                {
                    onetag = simpletag;

                    double tfidf_value_norm = 0.0;
                    double tfidf_value_mod = 0.0;
                    double socialtfidf_value_norm = 0.0;
                    double socialtfidf_value_mod = 0.0;
                    double probabilistic_value_norm = 0.0;
                    double probabilistic_value_mod = 0.0;


                    try
                    {
                        tfidf_value_norm = mydescTagsListList[Constants.devsetkeywords_tfidf][secIndex].TagDictionary[onetag];
                        socialtfidf_value_norm = mydescTagsListList[Constants.devsetkeywords_social_tfidf][secIndex].TagDictionary[onetag];
                        probabilistic_value_norm = mydescTagsListList[Constants.devsetkeywords_probabilistic][secIndex].TagDictionary[onetag];
                    }
                    catch (Exception ex) // this can occure for instance, if there is no tag for an image
                    {
                        tfidf_value_norm = 0.0;
                        socialtfidf_value_norm = 0.0;
                        probabilistic_value_norm = 0.0;
                    }
                    tfidf_value_norm_max = (tfidf_value_norm > tfidf_value_norm_max ? tfidf_value_norm : tfidf_value_norm_max);
                    socialtfidf_value_norm_max = (socialtfidf_value_norm > socialtfidf_value_norm_max ? socialtfidf_value_norm : socialtfidf_value_norm_max);
                    probabilistic_value_norm_max = (probabilistic_value_norm > probabilistic_value_norm_max ? probabilistic_value_norm : probabilistic_value_norm_max);

                    try
                    {
                        tfidf_value_mod = mydescTagsListList[Constants.mod_devsetkeywords_tfidf][secIndex].TagDictionary[onetag];
                        socialtfidf_value_mod = mydescTagsListList[Constants.mod_devsetkeywords_social_tfidf][secIndex].TagDictionary[onetag];
                        probabilistic_value_mod = mydescTagsListList[Constants.mod_devsetkeywords_probabilistic][secIndex].TagDictionary[onetag];
                    }
                    catch (Exception ex)
                    {

                        try
                        {
                            tfidf_value_mod = mydescTagsListList[Constants.devsetkeywords_tfidf][secIndex].TagDictionary[onetag];
                            socialtfidf_value_mod = mydescTagsListList[Constants.devsetkeywords_social_tfidf][secIndex].TagDictionary[onetag];
                            probabilistic_value_mod = mydescTagsListList[Constants.devsetkeywords_probabilistic][secIndex].TagDictionary[onetag];
                        }

                        catch (Exception ex2) // this can occure for instance, if there is no tag for an image
                        {
                            tfidf_value_mod = 0.0;
                            socialtfidf_value_norm = 0.0;
                            probabilistic_value_mod = 0.0;
                        }
                    }

                    tfidf_value_mod_max = (tfidf_value_mod > tfidf_value_mod_max ? tfidf_value_mod : tfidf_value_mod_max);
                    socialtfidf_value_mod_max = (socialtfidf_value_mod > socialtfidf_value_mod_max ? socialtfidf_value_mod : socialtfidf_value_mod_max);
                    probabilistic_value_mod_max = (probabilistic_value_mod > probabilistic_value_mod_max ? probabilistic_value_mod : probabilistic_value_mod_max);

                    tfidf_summa_norm += tfidf_value_norm;
                    tfidf_summa_mod += tfidf_value_mod;
                    socialtfidf_summa_norm += socialtfidf_value_norm;
                    socialtfidf_summa_mod += socialtfidf_value_mod;
                    probabilistic_summa_norm += probabilistic_value_norm;
                    probabilistic_summa_mod += probabilistic_value_mod;
                }
                tfidf_mean_norm = tfidf_summa_norm / counter;
                tfidf_mean_mod = tfidf_summa_mod / counter;
                socialtfidf_mean_norm = socialtfidf_summa_norm / counter;
                socialtfidf_mean_mod = socialtfidf_summa_mod / counter;
                probabilistic_mean_norm = probabilistic_summa_norm / counter;
                probabilistic_mean_mod = probabilistic_summa_mod / counter;

                // weighted using the max value
                // if there is no tag for an image, all the values are 0, then then Log does not make any sense
                if (tfidf_mean_norm != 0)
                {
                    tfidf_mean_norm_maxweight = tfidf_value_norm_max + Math.Log(tfidf_mean_norm);
                    tfidf_mean_mod_maxweight = tfidf_value_mod_max + Math.Log(tfidf_mean_mod);
                    socialtfidf_mean_norm_maxweight = socialtfidf_value_norm_max + Math.Log(socialtfidf_mean_norm);
                    socialtfidf_mean_mod_maxweight = socialtfidf_value_mod_max + Math.Log(socialtfidf_mean_mod);
                    probabilistic_mean_norm_maxweight = probabilistic_value_norm_max + Math.Log(probabilistic_mean_norm);
                    probabilistic_mean_mod_maxweight = probabilistic_value_mod_max + Math.Log(probabilistic_mean_mod);
                }
                else
                {
                    tfidf_mean_norm_maxweight = 0;
                    tfidf_mean_mod_maxweight = 0;
                    socialtfidf_mean_norm_maxweight = 0;
                    socialtfidf_mean_mod_maxweight = 0;
                    probabilistic_mean_norm_maxweight = 0;
                    probabilistic_mean_mod_maxweight = 0;
                }
                imgR.ID = simpleimage.ID;
                imgR.DateTaken = simpleimage.DateTaken;
                imgR.Description = simpleimage.Description;
                imgR.ImgCN = simpleimage.ImgCN;
                imgR.ImgFace = simpleimage.ImgFace;
                imgR.Latitude = simpleimage.Latitude;
                imgR.Longitude = simpleimage.Longitude;
                imgR.NbOfComments = simpleimage.NbOfComments;
                imgR.License = simpleimage.License;
                imgR.Url_b = simpleimage.Url_b;
                imgR.Username = simpleimage.Username;
                imgR.Views = simpleimage.Views;

                imgR.Add(tfidf_mean_norm, tfidf_mean_mod, socialtfidf_mean_norm, socialtfidf_mean_mod, probabilistic_mean_norm, probabilistic_mean_mod,
                    tfidf_mean_norm_maxweight, tfidf_mean_mod_maxweight, socialtfidf_mean_norm_maxweight, socialtfidf_mean_mod_maxweight, probabilistic_mean_norm_maxweight, probabilistic_mean_mod_maxweight);
                ListImgR.Add(imgR);
            }
            locRes.Add(ListImgR);

            return locRes;
        }




        public static LocationImageTags LocImgTagsCreator(OneLocation location)
        {
            // this contains one location(TITLE) and the list of tags related to the images of the location

            OneLocation myLocation = location;

            string[] splittedTags;
            int indexOfLocation = 0;


            LocationImageTags locImgTags = new LocationImageTags();
            List<imageTagsList> imageTagsListList = new List<imageTagsList>();
            for (int i = 0; i < myLocation.Images.Count; i++)
            {
                //if IRONPYTHON does not work:
                splittedTags = myLocation.Images[i].Tags.Split(' ');
               
                // First we have to set up IronPython on MONO
                //splittedTags = framework.WordDivider.divide(myLocation.Images[i].Tags.Split(' '));
                
                List<String> tags = new List<String>();
                imageTagsList imageTagsList = new imageTagsList();
                for (int j = 0; j < splittedTags.Length; j++)
                {

                    tags.Add(splittedTags[j]);
                }
                imageTagsList.ID = myLocation.Images[i].ID;
                imageTagsList.Rank = myLocation.Images[i].Rank;
                imageTagsList.DateTaken = myLocation.Images[i].DateTaken;
                imageTagsList.Description = myLocation.Images[i].Description;
                imageTagsList.Latitude = myLocation.Images[i].Latitude;
                imageTagsList.License = myLocation.Images[i].License;
                imageTagsList.Longitude = myLocation.Images[i].Longitude;
                imageTagsList.NbOfComments = myLocation.Images[i].NbOfComments;
                imageTagsList.Views = myLocation.Images[i].Views;
                imageTagsList.Title = myLocation.Images[i].Title;
                imageTagsList.Url_b = myLocation.Images[i].Url_b;
                imageTagsList.Username = myLocation.Images[i].Username;
                imageTagsList.ImgCN = myLocation.Images[i].ImgCN;
                imageTagsList.ImgFace = myLocation.Images[i].ImgFace;

                imageTagsList.Add(tags);
                imageTagsListList.Add(imageTagsList);
            }
            locImgTags.Title = myLocation.Title;
            locImgTags.Num = myLocation.Num;
            locImgTags.Latitude = myLocation.Latitude;
            locImgTags.Longitude = myLocation.Longitude;
            locImgTags.Wiki = myLocation.Wiki;
            locImgTags.Add(indexOfLocation, imageTagsListList);

            return locImgTags;
        }
    }

    class LocationResults : framework.OneLocation
    {

        public List<ImageResults> ListImgR { get; set; }

        public LocationResults()
        {

            this.ListImgR = new List<ImageResults>();
        }
        public void Add(List<ImageResults> ListImgR)
        {

            this.ListImgR = ListImgR;
        }
    }


    class ImageResults : framework.OneImage
    {

        public double tfidf_mean_norm { get; set; }
        public double tfidf_mean_mod { get; set; }
        public double socialtfidf_mean_norm { get; set; }
        public double socialtfidf_mean_mod { get; set; }
        public double probabilistic_mean_norm { get; set; }
        public double probabilistic_mean_mod { get; set; }
        public double tfidf_mean_norm_maxweight { get; set; }
        public double tfidf_mean_mod_maxweight { get; set; }
        public double socialtfidf_mean_norm_maxweight { get; set; }
        public double socialtfidf_mean_mod_maxweight { get; set; }
        public double probabilistic_mean_norm_maxweight { get; set; }
        public double probabilistic_mean_mod_maxweight { get; set; }
        public double rank_weight { get; set; }

        public ImageResults(
            double tfidf_mean_norm = 0.0,
            double tfidf_mean_mod = 0.0,
            double socialtfidf_mean_norm = 0.0,
            double socialtfidf_mean_mod = 0.0,
            double probabilistic_mean_norm = 0.0,
            double probabilistic_mean_mod = 0.0,
            double tfidf_mean_norm_maxweight = 0.0,
            double tfidf_mean_mod_maxweight = 0.0,
            double socialtfidf_mean_norm_maxweight = 0.0,
            double socialtfidf_mean_mod_maxweight = 0.0,
            double probabilistic_mean_norm_maxweight = 0.0,
            double probabilistic_mean_mod_maxweight = 0.0,
            double rank_weight = 0.0)
        {

            this.tfidf_mean_norm = tfidf_mean_norm;
            this.tfidf_mean_mod = tfidf_mean_mod;
            this.socialtfidf_mean_norm = socialtfidf_mean_norm;
            this.socialtfidf_mean_mod = socialtfidf_mean_mod;
            this.probabilistic_mean_norm = probabilistic_mean_norm;
            this.probabilistic_mean_mod = probabilistic_mean_mod;
            this.tfidf_mean_norm_maxweight = tfidf_mean_norm_maxweight;
            this.tfidf_mean_mod_maxweight = tfidf_mean_mod_maxweight;
            this.socialtfidf_mean_norm_maxweight = socialtfidf_mean_norm_maxweight;
            this.socialtfidf_mean_mod_maxweight = socialtfidf_mean_mod_maxweight;
            this.probabilistic_mean_norm_maxweight = probabilistic_mean_norm_maxweight;
            this.probabilistic_mean_mod_maxweight = probabilistic_mean_mod_maxweight;
            this.rank_weight = rank_weight;
        }
        public void Add(
            double tfidf_mean_norm,
            double tfidf_mean_mod,
            double socialtfidf_mean_norm,
            double socialtfidf_mean_mod,
            double probabilistic_mean_norm,
            double probabilistic_mean_mod,
            double tfidf_mean_norm_maxweight,
            double tfidf_mean_mod_maxweight,
            double socialtfidf_mean_norm_maxweight,
            double socialtfidf_mean_mod_maxweight,
            double probabilistic_mean_norm_maxweight,
            double probabilistic_mean_mod_maxweight)
        {
            this.tfidf_mean_norm = tfidf_mean_norm;
            this.tfidf_mean_mod = tfidf_mean_mod;
            this.socialtfidf_mean_norm = socialtfidf_mean_norm;
            this.socialtfidf_mean_mod = socialtfidf_mean_mod;
            this.probabilistic_mean_norm = probabilistic_mean_norm;
            this.probabilistic_mean_mod = probabilistic_mean_mod;
            this.tfidf_mean_norm_maxweight = tfidf_mean_norm_maxweight;
            this.tfidf_mean_mod_maxweight = tfidf_mean_mod_maxweight;
            this.socialtfidf_mean_norm_maxweight = socialtfidf_mean_norm_maxweight;
            this.socialtfidf_mean_mod_maxweight = socialtfidf_mean_mod_maxweight;
            this.probabilistic_mean_norm_maxweight = probabilistic_mean_norm_maxweight;
            this.probabilistic_mean_mod_maxweight = probabilistic_mean_mod_maxweight;
        }

        public void setNewRank(double rank_weight)
        {
            this.rank_weight = rank_weight;
        }
    }

    public class LocationImageTags : framework.OneLocation
    {
        //this class contains the name of a location, and the taglists for all the images which belongs to that location

        public int locIndex { get; set; }
        public List<imageTagsList> imagetagsListList { get; set; }

        public LocationImageTags(int locIndex = -1)
        {

            this.locIndex = locIndex;
            this.imagetagsListList = new List<imageTagsList>();

        }
        public void Add(int locIndex = -1, List<imageTagsList> imagetagsListList = null)
        {
            this.locIndex = locIndex;
            this.imagetagsListList = imagetagsListList;

        }

    }


    public class imageTagsList : framework.OneImage
    {


        public List<String> imgtags { get; set; }

        public imageTagsList()
        {

            this.imgtags = new List<String>();
        }
        public void Add(List<String> imgtags = null)
        {
            this.imgtags = imgtags;
        }

    }


    class IndexKeeper
    {

 
        //Location = TagsListList[TagsListListFirstIndex][TextDescSecondIndex]

        public int TextDescSecondIndex { get; set; }

        public IndexKeeper(int TextDescSecondIndex = -1)
        {
            this.TextDescSecondIndex = TextDescSecondIndex;
        }

        static public IndexKeeper FindIndex(OneLocation location, List<List<descTags>> descTagsListList)
        {
            //finds the indexes for the same location in the locations and descTagListList

            List<List<descTags>> mydescTagsListList = descTagsListList;
            OneLocation myLocation = location;
            IndexKeeper ik = new IndexKeeper();

            for (int k = 0; k < mydescTagsListList[0].Count; k++)
            {
                if (mydescTagsListList[0][k].Name == myLocation.Title)
                {
                    ik.TextDescSecondIndex = k;
                    break;
                }

            }

            return ik;

        }

        static public int getTextDescSecondIndex(IndexKeeper ListIndexKeeper)
        {

            IndexKeeper myListIndexKeeper = ListIndexKeeper;

            int TextDescSecondIndex = myListIndexKeeper.TextDescSecondIndex;
            return TextDescSecondIndex;
        }

    }


}