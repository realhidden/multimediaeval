﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace desctext_reader
{
    class DescTextAlgorithm
    {

        static public List<framework.OneImage> ImageRankByDescText(framework.OneLocation keyword)
        {

			double[] weights = { 0, 0, 0, 0, 0, 1, 0 };
            List<framework.OneImage> result = new List<framework.OneImage>();
            framework.OneLocation resultLoc = keyword;
            List<List<descTags>> descTagsListList = DescTextLocTagValueBuilder.descTagsReader();

            // LocImgValue contain all the informations about the location/images  : tfidf, socialtfidf average values, ...
            LocationResults LocImgValue = ImageValueBuilder.BuildImageValues(keyword, descTagsListList);

            double[] myWeights = weights;

            LocationResults location = ImageValueBuilder.BuildImageValues(keyword, descTagsListList);


            List<ImageResults> imgr_ordered_list = new List<ImageResults>();

            
            // here are the weigths to calculate the final image.rank_weight
            double weight_tfidf_mean_norm = myWeights[0];
            double weight_tfidf_mean_mod = myWeights[1];
            double weight_socialtfidf_mean_norm = myWeights[2];
            double weight_socialtfidf_mean_mod = myWeights[3];
            double weight_probabilistic_mean_norm = myWeights[4];
            double weight_probabilistic_mean_mod = myWeights[5];
            double weight_rank = myWeights[6];


            //calculate the final rank_weight for every image
            // IDE MÉG KELL VALAMI, MERT MOST NEM TÚL JÓ A SÚLYOZÁS
            foreach (var image in location.ListImgR)
            {
                
                image.rank_weight = weight_tfidf_mean_norm * image.tfidf_mean_norm_maxweight
                                    + weight_tfidf_mean_mod * image.tfidf_mean_mod_maxweight
                                    + weight_socialtfidf_mean_norm * image.socialtfidf_mean_norm_maxweight
                                    + weight_socialtfidf_mean_mod * image.socialtfidf_mean_mod_maxweight
                                    + weight_probabilistic_mean_norm * image.probabilistic_mean_norm_maxweight
                                    + weight_probabilistic_mean_mod * image.probabilistic_mean_mod_maxweight
                                    + weight_rank * image.Rank;

            }


            imgr_ordered_list = location.ListImgR.OrderByDescending(a => (a.rank_weight)).ToList();

            int newRank = 1;
            
            foreach (var image in imgr_ordered_list)
            {
                framework.OneImage resultImg = new framework.OneImage();
                resultImg.ID = image.ID;
                resultImg.DateTaken = image.DateTaken;
                resultImg.Description = image.Description;
                resultImg.ImgCN = image.ImgCN;
                resultImg.ImgFace = image.ImgFace;
                resultImg.Latitude = image.Latitude;
                resultImg.License = image.License;
                resultImg.Longitude = image.Longitude;
                resultImg.NbOfComments = image.NbOfComments;
                resultImg.Tags = image.Tags;
                resultImg.Title = image.Title;
                resultImg.Url_b = image.Url_b;
                resultImg.Username = image.Username;
                resultImg.Views = image.Views;
                resultImg.Rank = newRank;
                newRank += 1;
                result.Add(resultImg);
            }

            return result;

        }


    }

  

}
