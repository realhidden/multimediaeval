#/bin/sh

# HOW TO RUN:
# parameters: <BASE data set dir like:devset/keywords> <result dir inside results> <locationid> <locationname>
#
# LIKE: ./score.sh "devset/keywords" "" 27 "Basilica of Saint Peter Vatican"


# create tmp
tempfoo=`basename $0`
TP=`mktemp -q /tmp/${tempfoo}.XXXXXX`
TP2=`mktemp -q /tmp/${tempfoo}.XXXXXX`

#generating stuff
cat > $TP << EOF
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<topics>
<topic>
	<number>$3</number>
	<title>$4</title>
	<latitude>0</latitude>
	<longitude>0</longitude>
	<wiki>http://</wiki>
</topic>
</topics>
EOF

#running
java -jar div_eval.jar -r "../../results/$2/$4.out" -rgt ../../dataset/$1/gt/rGT -dgt ../../dataset/$1/gt/dGT -t $TP -o / -f $TP2 > /dev/null

#nice hack to run the result line to stdout
head -9 $TP2 | tail -1

#remove tmp
rm $TP
rm $TP2