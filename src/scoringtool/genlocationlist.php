<?php
error_reporting(0);
// Load location xml and generate tester sh for that

function toArray($xml) {
        $array = json_decode(json_encode($xml), TRUE);
        
        foreach ( array_slice($array, 0) as $key => $value ) {
            if ( empty($value) ) $array[$key] = NULL;
            elseif ( is_array($value) ) $array[$key] = toArray($value);
        }

        return $array;
    }

$locs=toArray(simplexml_load_string(file_get_contents($argv[1])));


echo("#!/bin/sh\n#Generated score.sh wrapper from: ".array_pop(explode("/",$argv[1]))."\n\n");
foreach($locs["topic"] as $onLoc)
{
	echo("./score.sh \"$1\" \"$2\" ");
	echo($onLoc['number']);
	echo(" \"".$onLoc['title']."\"\n");
}
?>