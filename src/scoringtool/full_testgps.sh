#!/bin/sh
#Generated score.sh wrapper from: testsetkeywordsGPS_topics.xml

./score.sh "$1" "$2" 51 "African Burial Ground"
./score.sh "$1" "$2" 52 "Agra Fort"
./score.sh "$1" "$2" 53 "Ahu Tongariki"
./score.sh "$1" "$2" 54 "Ajanta Caves"
./score.sh "$1" "$2" 55 "Al Aqsa Mosque"
./score.sh "$1" "$2" 56 "Alabama State Capitol"
./score.sh "$1" "$2" 57 "Albert Memorial"
./score.sh "$1" "$2" 58 "Alte Nationalgalerie"
./score.sh "$1" "$2" 59 "Altes Museum"
./score.sh "$1" "$2" 60 "Amiens Cathedral"
./score.sh "$1" "$2" 61 "Angkor Wat"
./score.sh "$1" "$2" 62 "Aqueduct of Segovia"
./score.sh "$1" "$2" 63 "Ara Pacis"
./score.sh "$1" "$2" 64 "Arc de Triomf"
./score.sh "$1" "$2" 65 "Arc de Triomphe"
./score.sh "$1" "$2" 66 "Arc de triomphe du Carrousel"
./score.sh "$1" "$2" 67 "Arch of Constantine"
./score.sh "$1" "$2" 68 "Arch of Septimius Severus"
./score.sh "$1" "$2" 69 "Arch of Titus"
./score.sh "$1" "$2" 70 "Argentine National Congress"
./score.sh "$1" "$2" 71 "Asinelli tower"
./score.sh "$1" "$2" 72 "Atomium in Brussels"
./score.sh "$1" "$2" 73 "Aztec Ruins"
./score.sh "$1" "$2" 74 "Basilica di San Zeno"
./score.sh "$1" "$2" 75 "Berlin Cathedral"
./score.sh "$1" "$2" 76 "Bibliotheque Nationale de France"
./score.sh "$1" "$2" 77 "Bode Museum"
./score.sh "$1" "$2" 78 "Bok Tower Gardens"
./score.sh "$1" "$2" 79 "Brandenburg Gate in Berlin"
./score.sh "$1" "$2" 80 "Buenos Aires Cabildo"
./score.sh "$1" "$2" 81 "Buenos Aires Central Post Office"
./score.sh "$1" "$2" 82 "Buenos Aires Metropolitan Cathedral"
./score.sh "$1" "$2" 83 "Cabrillo"
./score.sh "$1" "$2" 84 "Capitole de Toulouse"
./score.sh "$1" "$2" 85 "Casa Batllo"
./score.sh "$1" "$2" 86 "Casa de Pilatos"
./score.sh "$1" "$2" 87 "Casa Grande Ruins"
./score.sh "$1" "$2" 88 "Casa Rosada"
./score.sh "$1" "$2" 89 "Castel Nuovo or Maschio Angioino"
./score.sh "$1" "$2" 90 "Castello del Valentino"
./score.sh "$1" "$2" 91 "Castillo de San Marcos"
./score.sh "$1" "$2" 92 "Castle Clinton"
./score.sh "$1" "$2" 93 "Cathedral of Santiago de Compostela"
./score.sh "$1" "$2" 94 "Charing Cross"
./score.sh "$1" "$2" 95 "Chartres Cathedral"
./score.sh "$1" "$2" 96 "Chicago Board of Trade Building"
./score.sh "$1" "$2" 97 "Chichen Itza"
./score.sh "$1" "$2" 98 "Christ the Redeemer"
./score.sh "$1" "$2" 99 "Civic Center San Francisco"
./score.sh "$1" "$2" 100 "Cologne Cathedral"
./score.sh "$1" "$2" 101 "Colonne Vendome"
./score.sh "$1" "$2" 102 "Colosseum or Coliseum"
./score.sh "$1" "$2" 103 "Columbus Monument"
./score.sh "$1" "$2" 104 "Conciergerie"
./score.sh "$1" "$2" 105 "Corral del Carbon"
./score.sh "$1" "$2" 106 "Doge's Palace"
./score.sh "$1" "$2" 107 "Duomo or St Mary of the Assumption"
./score.sh "$1" "$2" 108 "Eiffel Tower"
./score.sh "$1" "$2" 109 "El Angel"
./score.sh "$1" "$2" 110 "Elephanta Caves"
./score.sh "$1" "$2" 111 "Elogio del Horizonte"
./score.sh "$1" "$2" 112 "Ephesus"
./score.sh "$1" "$2" 113 "Fatehpur Sikri"
./score.sh "$1" "$2" 114 "Florence Baptistry"
./score.sh "$1" "$2" 115 "Fontaine des Innocents"
./score.sh "$1" "$2" 116 "Fontaine Saint-Sulpice"
./score.sh "$1" "$2" 117 "Fontana dei Quattro Fiumi"
./score.sh "$1" "$2" 118 "Forbidden City"
./score.sh "$1" "$2" 119 "Fossar de les Moreres"
./score.sh "$1" "$2" 120 "Fox Theatre"
./score.sh "$1" "$2" 121 "Freedom Tower"
./score.sh "$1" "$2" 122 "Gateway Arch"
./score.sh "$1" "$2" 123 "George Washington Carver"
./score.sh "$1" "$2" 124 "Georgia State Capitol"
./score.sh "$1" "$2" 125 "Giotto's Campanile"
./score.sh "$1" "$2" 126 "Golden Gate Bridge in San Francisco"
./score.sh "$1" "$2" 127 "Grand Palais"
./score.sh "$1" "$2" 128 "Grande Arche"
./score.sh "$1" "$2" 129 "Great Pyramid of Giza"
./score.sh "$1" "$2" 130 "Guggenheim Museum Bilbao"
./score.sh "$1" "$2" 131 "Hagia Sophia in Istanbul"
./score.sh "$1" "$2" 132 "Hoover Dam"
./score.sh "$1" "$2" 133 "Hospital de Sant Pau"
./score.sh "$1" "$2" 134 "Humayun Tomb"
./score.sh "$1" "$2" 135 "Iguazu Falls"
./score.sh "$1" "$2" 136 "Iolani Palace"
./score.sh "$1" "$2" 137 "Jin Mao tower"
./score.sh "$1" "$2" 138 "Jokhang"
./score.sh "$1" "$2" 139 "Karlstor"
./score.sh "$1" "$2" 140 "Khajuraho Group of Monuments"
./score.sh "$1" "$2" 141 "Kronborg Castle"
./score.sh "$1" "$2" 142 "La Mezquita"
./score.sh "$1" "$2" 143 "La Rambla"
./score.sh "$1" "$2" 144 "La Sainte Chapelle"
./score.sh "$1" "$2" 145 "Liberty Memorial"
./score.sh "$1" "$2" 146 "Lincoln Home"
./score.sh "$1" "$2" 147 "Lincoln Tomb"
./score.sh "$1" "$2" 148 "Longmen Grottoes"
./score.sh "$1" "$2" 149 "Louisiana State Capitol"
./score.sh "$1" "$2" 150 "Luxembourg Palace"
./score.sh "$1" "$2" 151 "Machu Picchu"
./score.sh "$1" "$2" 152 "Mahabodhi Temple"
./score.sh "$1" "$2" 153 "Marble Arch"
./score.sh "$1" "$2" 154 "Mark Twain House"
./score.sh "$1" "$2" 155 "Mausoleum of Hadrian"
./score.sh "$1" "$2" 156 "Milan Cathedral"
./score.sh "$1" "$2" 157 "Mission San Carlos Borromeo de Carmelo"
./score.sh "$1" "$2" 158 "Mole Antonelliana"
./score.sh "$1" "$2" 159 "Mont Saint-Michel"
./score.sh "$1" "$2" 160 "Monument to the Great Fire of London"
./score.sh "$1" "$2" 161 "Monument to the Independence of Brazil"
./score.sh "$1" "$2" 162 "Moulin Rouge"
./score.sh "$1" "$2" 163 "Nebraska State Capitol"
./score.sh "$1" "$2" 164 "Nelson's Column"
./score.sh "$1" "$2" 165 "Neuschwanstein Castle"
./score.sh "$1" "$2" 166 "Notre Dame de Paris"
./score.sh "$1" "$2" 167 "Obelisco"
./score.sh "$1" "$2" 168 "Pachacamac"
./score.sh "$1" "$2" 169 "Palace of Versailles"
./score.sh "$1" "$2" 170 "Palacio Barolo"
./score.sh "$1" "$2" 171 "Palacio de la Magdalena"
./score.sh "$1" "$2" 172 "Palau Guell"
./score.sh "$1" "$2" 173 "Palazzo Carignano"
./score.sh "$1" "$2" 174 "Palazzo d'Accursio"
./score.sh "$1" "$2" 175 "Palazzo Madama"
./score.sh "$1" "$2" 176 "Palazzo Pubblico"
./score.sh "$1" "$2" 177 "Palazzo Rosso"
./score.sh "$1" "$2" 178 "Palazzo Vecchio"
./score.sh "$1" "$2" 179 "Pantheon"
./score.sh "$1" "$2" 180 "Piazza dei Signori"
./score.sh "$1" "$2" 181 "Piazza del Campo"
./score.sh "$1" "$2" 182 "Piazza del Plebiscito"
./score.sh "$1" "$2" 183 "Piazza della Signoria"
./score.sh "$1" "$2" 184 "Piazza Navona"
./score.sh "$1" "$2" 185 "Piazza Sant'Oronzo"
./score.sh "$1" "$2" 186 "Piazza Vittorio Veneto"
./score.sh "$1" "$2" 187 "Piccadilly Circus"
./score.sh "$1" "$2" 188 "Pitti Palace"
./score.sh "$1" "$2" 189 "Place de la Concorde"
./score.sh "$1" "$2" 190 "Place de la Republique"
./score.sh "$1" "$2" 191 "Place des Vosges"
./score.sh "$1" "$2" 192 "Plantin-Moretus Museum"
./score.sh "$1" "$2" 193 "Plaza de Cibeles"
./score.sh "$1" "$2" 194 "Plaza de Toros de Las Ventas"
./score.sh "$1" "$2" 195 "Pointe de Pen-Hir"
./score.sh "$1" "$2" 196 "Pont du Gard"
./score.sh "$1" "$2" 197 "Pont Neuf"
./score.sh "$1" "$2" 198 "Ponte Vecchio"
./score.sh "$1" "$2" 199 "Potala Palace"
./score.sh "$1" "$2" 200 "Puerta de Alcala"
./score.sh "$1" "$2" 201 "Puerta del Sol"
./score.sh "$1" "$2" 202 "Qutb complex"
./score.sh "$1" "$2" 203 "Red Fort"
./score.sh "$1" "$2" 204 "Reims Cathedral"
./score.sh "$1" "$2" 205 "Rialto Bridge"
./score.sh "$1" "$2" 206 "Rila Monastery"
./score.sh "$1" "$2" 207 "Roman Forum"
./score.sh "$1" "$2" 208 "Romanian Athenaeum"
./score.sh "$1" "$2" 209 "Royal Palace of Madrid"
./score.sh "$1" "$2" 210 "Royal Palace of Naples"
./score.sh "$1" "$2" 211 "Sacre Coeur"
./score.sh "$1" "$2" 212 "Sagrada Familia"
./score.sh "$1" "$2" 213 "Saint Mark's Basilica"
./score.sh "$1" "$2" 214 "Saint Mary of the Flower"
./score.sh "$1" "$2" 215 "Saint-Jacques Tower"
./score.sh "$1" "$2" 216 "Santa Maria della Spina"
./score.sh "$1" "$2" 217 "Santa Maria Novella"
./score.sh "$1" "$2" 218 "Schonbrunn Palace"
./score.sh "$1" "$2" 219 "Seville Cathedral"
./score.sh "$1" "$2" 220 "Sforza Castle"
./score.sh "$1" "$2" 221 "Sonoma Plaza"
./score.sh "$1" "$2" 222 "Spanish Steps"
./score.sh "$1" "$2" 223 "Speyer Cathedral"
./score.sh "$1" "$2" 224 "Split Rock Lighthouse"
./score.sh "$1" "$2" 225 "SS Jeremiah O'Brien"
./score.sh "$1" "$2" 226 "St. Basil Cathedral"
./score.sh "$1" "$2" 227 "Statue of George IV"
./score.sh "$1" "$2" 228 "Statue of King Charles I"
./score.sh "$1" "$2" 229 "Statue of Liberty"
./score.sh "$1" "$2" 230 "Statue of Oliver Cromwell"
./score.sh "$1" "$2" 231 "Stonehenge"
./score.sh "$1" "$2" 232 "Stravinsky Fountain"
./score.sh "$1" "$2" 233 "Summer Palace"
./score.sh "$1" "$2" 234 "Sydney Opera House"
./score.sh "$1" "$2" 235 "Teatro Real"
./score.sh "$1" "$2" 236 "Teotihuacan"
./score.sh "$1" "$2" 237 "Terracotta Army"
./score.sh "$1" "$2" 238 "The Blue Mosque"
./score.sh "$1" "$2" 239 "The Burj al Arab Hotel"
./score.sh "$1" "$2" 240 "The Henry Ford"
./score.sh "$1" "$2" 241 "The Little Mermaid"
./score.sh "$1" "$2" 242 "The Taj Mahal in Agra"
./score.sh "$1" "$2" 243 "Torre del Mangia"
./score.sh "$1" "$2" 244 "Torre Picasso"
./score.sh "$1" "$2" 245 "Torres KIO"
./score.sh "$1" "$2" 246 "Tour Montparnasse"
./score.sh "$1" "$2" 247 "Tower Bridge London"
./score.sh "$1" "$2" 248 "Tower of Belem"
./score.sh "$1" "$2" 249 "Trafalgar Square"
./score.sh "$1" "$2" 250 "Trajan's Column"
./score.sh "$1" "$2" 251 "Trajan's Forum"
./score.sh "$1" "$2" 252 "Trevi Fountain"
./score.sh "$1" "$2" 253 "Uffizi Gallery"
./score.sh "$1" "$2" 254 "United States Capitol"
./score.sh "$1" "$2" 255 "Uxmal"
./score.sh "$1" "$2" 256 "Valle de los Caidos"
./score.sh "$1" "$2" 257 "Verona Arena"
./score.sh "$1" "$2" 258 "Victor Emmanuel II"
./score.sh "$1" "$2" 259 "Victoria Memorial"
./score.sh "$1" "$2" 260 "Wartburg Castle"
./score.sh "$1" "$2" 261 "Xochicalco"
