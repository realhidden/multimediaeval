#include <opencv/highgui.h>
#include <opencv/cv.h>
#include <stdio.h>
const char* cascade_name = "haarcascade_frontalface_alt.xml";

int main(int argc, const char** argv)
{
	IplImage *img = cvLoadImage(argv[1]);

	//Haar + memory
	CvMemStorage* storage = cvCreateMemStorage(0);
	CvHaarClassifierCascade* cascade = (CvHaarClassifierCascade*)cvLoad( cascade_name, 0, 0, 0 );

	if( !cascade )
	{
		fprintf( stderr, "ERROR: Could not load classifier cascade\n" );
		return -1;
	}

	//detect
	CvSeq* faces = cvHaarDetectObjects( img, cascade, storage, 1.1, 2, CV_HAAR_DO_CANNY_PRUNING, cvSize(40, 40) );
	
	// size calculator
	double allsize=img->width*img->height;
	double thesize=allsize;

	// Loop the number of faces found.
	for( int i = 0; i < (faces ? faces->total : 0); i++ )
	{
		CvRect* r = (CvRect*)cvGetSeqElem( faces, i );
		//printf("Face %d : %d %d\n",i,r->width,r->height);
		thesize-=r->width*r->height;
	}

	if (thesize==allsize)
	{
		printf("0");
	}else if (thesize<0.0f)
	{
		printf("1");
	}else{
		printf("%lf",1.0f-thesize/allsize);
	}
	cvReleaseImage(&img);
	return 0;
}
