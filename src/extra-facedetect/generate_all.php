<?php
function getAllFiles($dir)
{
	$ret=array();
	if ($h=@opendir($dir))
	{
		while (false !== ($entry = readdir($h))) {
        	if ($entry != "." && $entry != "..") {
            	$ret[]=$dir."/".$entry;
        	}
    	}
    	closedir($h);
    }
    return $ret;
}

function recursive_mkdir($path, $mode = 0777) {
    $dirs = explode(DIRECTORY_SEPARATOR , $path);
    $count = count($dirs);
    $path = '.';
    for ($i = 0; $i < $count; ++$i) {
        $path .= DIRECTORY_SEPARATOR . $dirs[$i];
        if (!is_dir($path) && !mkdir($path, $mode)) {
            return false;
        }
    }
    return true;
}

//all dirs
$sets=array("devset/keywords","devset/keywordsGPS","testset/keywords","testset/keywordsGPS");

//export dir in docs
$exportdir="../../docs/face-csv/";

//for each set
echo("Generating 'location FACE.csv' files into descvis. This takes time.\n");
foreach($sets as $oneset)
{
	echo("Processing $oneset\n");
	$xmls=getAllFiles("../../dataset/".$oneset."/xml");

	foreach($xmls as $onexml)
	{
		//grab filename + strip xml from end
		$onexml=explode("/",$onexml);
		$onexml=array_pop($onexml);
		if (strpos($onexml,".xml")==FALSE)
			continue;
		$onexml=substr($onexml,0,-4);

		//here we got a location
		$imgs=getAllFiles("../../dataset/".$oneset."/img/".$onexml);
		echo("\t".$onexml." (".count($imgs).") ");

		//for each location, generate one file in DOCS dir
		$csvfile="";

		foreach($imgs as $oneimg)
		{
			//grab imgid & ignore nonjpgs
			$imgid=explode("/",$oneimg);
			$imgid=array_pop($imgid);
			if (strpos($imgid,".jpg")==FALSE)
				continue;
			$imgid=substr($imgid,0,-4);

			//execute external binary
			$result=exec("./facer \"".$oneimg."\"");
			echo(".");

			//add to csv file
			$csvfile.=$imgid.",".$result."\n";
		}
		echo("\n");

		//save to file
		recursive_mkdir($exportdir.$oneset."/descvis/img/");
		file_put_contents($exportdir.$oneset."/descvis/img/".$onexml." FACE.csv", $csvfile);
	}
	
}
?>