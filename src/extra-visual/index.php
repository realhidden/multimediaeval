<style>
html,body{
	padding:0;
	margin:0;
}
form{
	display: inline-block;
}
.oneimg{
	transition: all 2s ease;
	display: inline-block;
	width:200px;
	height:200px;
	text-align: center;
	
	position: absolute;
	top:40;
	left:0;
}
.oneimg img{
	max-height: 200px;
	max-width: 200px;
}
</style>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<?php
$basedir="../../dataset/";
$loc=array();
$solutions=array('flickr','cluster','me13div_BMEMTM2013_ok1visual','me13div_BMEMTM2013_ok2text','me13div_BMEMTM2013_ok3textvis');

function loadLocs($locfile)
{
	$buffer=file_get_contents($locfile);
	$obj = simplexml_load_string($buffer);
	$json = json_encode($obj, JSON_PRETTY_PRINT);
	return json_decode($json, TRUE);
}


// KW
$keywloc=loadLocs($basedir."testset/keywords/testsetkeywords_topics.xml");

foreach ($keywloc['topic'] as $l1)
{
	$loc['testset/keywords/'.$l1['number']."/".$l1['title']]=$l1['title'];
}

$keywgpsloc=loadLocs($basedir."testset/keywordsGPS/testsetkeywordsGPS_topics.xml");
foreach ($keywgpsloc['topic'] as $l1)
{
	$loc['testset/keywordsGPS/'.$l1['number']."/".$l1['title']]=$l1['title'];
}

// if location is set
$allimg=array();
$orders=array();

if (isset($_GET['loc']))
{
	$oneloc=explode('/',$_GET['loc']);
	$locdir=$basedir.$oneloc[0].'/'.$oneloc[1].'/img/'.$oneloc[3].'/';

	if ($handle = opendir($locdir)) {
    while (false !== ($entry = readdir($handle))) {
        if (!in_array($entry, array(".DS_Store",".","..")))
        {
        	$e=explode('.',$entry);
        	$allimg[$e[0]]=$locdir.$entry;
        }
    }
    closedir($handle);
	}

	//grab order for eachset
	foreach($solutions as $onesol)
	{
		$oneord=array();
		if ($onesol=='flickr')
		{
			$fli=file_get_contents($locdir."../../xml/".$oneloc[3].".xml");
			$obj = simplexml_load_string($fli);
			$json = json_encode($obj, JSON_PRETTY_PRINT);
			$fli=json_decode($json, TRUE);

			foreach($fli['photo'] as $onep)
			{
				$oneord[]=$onep["@attributes"]['id'];
				if (count($oneord)==50)
					break;
			}
		}else{
			$set=file_get_contents("../../dataset/result/".$onesol.".txt");
			$set=explode("\n",$set);
			foreach($set as $oneset)
			{
				$oneset=explode("\t",$oneset);
				if ($oneset[0]!=$oneloc[2])
					continue;
				$oneord[]=$oneset[2];
			}
		}

		$orders[$onesol]=$oneord;
	}
}
?>
<form method='get'>
<select name='loc'>
<?php
foreach($loc as $key=>$value)
{
	echo("<option value=\"".$key."\">".$value."</option>");
}
?>
</select>
<input type='submit'/>
</select>
</form>
<select id='sol'>
<?php
foreach($solutions as $s1)
{
	echo("<option value=\"".$s1."\">".$s1."</option>");
}
?>
</select>
<br/>
<?php
foreach($allimg as $id => $img)
{
	echo("<div class='oneimg'><img id='$id' src='$img'/></div>");
}
?>
<script>
var imgorders=<?=json_encode($orders,JSON_PRETTY_PRINT)?>;
var allimages=<?=json_encode(array_keys($allimg),JSON_PRETTY_PRINT)?>;

function setNewOrder(which)
{
	var neworder=imgorders[which];
	var cx=0;
	var cy=40;
	var mx=$(window).width();

	var hasimg=[];
	for (var i=0;i<neworder.length;i++)
	{
		$dom=$("#"+neworder[i]).parent();

		if (cx+$dom.width()<mx)
		{
			$dom.css({'top':cy,'left':cx,'opacity':1});
		}else{
			cx=0;
			cy+=$dom.height()+5;

			$dom.css({'top':cy,'left':cx,'opacity':1});
		}

		cx+=$dom.width()+5;
		hasimg.push(neworder[i]);
	}

	for (var i=0;i<allimages.length;i++)
	{
		if (hasimg.indexOf(""+allimages[i])==-1)
		{
			$dom=$("#"+allimages[i]).parent();
			$dom.css({'top':cy,'left':cx,'opacity':0});
		}
	}
}

$(function(){
	setNewOrder('flickr');
	$("#sol").change(function(){
		setNewOrder($(this).val());
	})
});
</script>