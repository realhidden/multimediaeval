#!/bin/sh
MYBIN="mono ./src/project/framework/framework/bin/Debug/framework.exe"
ALGO="noface"

mkdir -p results/$ALGO/devkw
$MYBIN -o results/$ALGO/devkw -d dataset/devset/keywords

mkdir -p results/$ALGO/devgps
$MYBIN -o results/$ALGO/devgps -d dataset/devset/keywordsGPS

#mkdir -p results/baseline/testkw
#$MYBIN -o results/baseline/testkw -d dataset/testset/keywords

#mkdir -p results/baseline/testgps
#$MYBIN -o results/baseline/testgps -d dataset/testset/keywordsGPS

#sh full_devkw.sh "devset/keywords" "baseline/devkw" > ../../results/baseline_devkw.csv
#sh full_devgps.sh "devset/keywordsGPS" "baseline/devgps" > ../../results/baseline_devgps.csv
