DiverseImages2013

Mindegyik könyvtárnak megvan a saját szerepe, ez alapján használjuk őket.

/dataset
	a kiadott seteket tároló könyvtár, tartalmát a git figyelmen kívül hagyja (!!!)

	A könyvtáron belül a következő szerkezetnek kell lennie (testset opcionális):
		/dataset/devset/keywords/... 		-	ide kicsomagolva a devsetkeywords.zip
		/dataset/devset/keywordsGPS/...		-	ide kicsomagolva a devsetkeywordsGPS.zip
		/dataset/testset/keywords/...		-	ide kicsomagolva a testsetkeywords.zip
		/dataset/testset/keywordsGPS/...	-	ide kicsomagolva a testsetkeywordsGPS.zip

	Ez alapján tehát léteznie kell a következő fájloknak például:
		/dataset/devset/keywords/devsetkeywords_topics.xml
		/dataset/devset/keywords/desctxt/devsetkeywords-tfidf.txt
		/dataset/devset/keywords/descvis/img/Abbey of Saint Gall CM3x3.csv
		/dataset/devset/keywords/imgwiki/Casas Grandes Chihuahua Mexico (Aromgom).jpg

/docs
	minden, ami dokumentáció vagy ahhoz kapcsolódik

/results
	futási eredményeket tároló könyvtár, tartalmát a git figyelmen kívül hagyja (!!!)

/src
	minden ami kód, bár Ákos fogja ennek strukturáját meghatározni, javaslat:

		/src/common/	-	általános funkciókat megvalósító osztályok fájljai (lib jelleggel)
		/src/project/	-	fejlesztőkörnyezet(ek) projektfájljai
		/src/xy.cs 		-  	egy adott submissiont / tesztet elvégző fájl, saját main függvénnyel

	Ha automatizáltan akarunk tesztelni, akkor el tudom képzelni, hogy az /src/*.cs fájlokat a szerver automatikusan lefordítja és teszteli, így megkönnyítve a munkánkat.

/readme.txt
	a repository legizgalmasabb fájlja